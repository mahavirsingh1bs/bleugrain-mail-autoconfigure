package com.bleugrain.mail.autoconfigure;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ConfigurationProperties(prefix = "bleugrain.mail")
public class MailProperties {
	private String host;
	private int port;
	private String username;
	private String password;

	private String transportProtocol = "smtp";
	private boolean smtpAuth = true;
	private boolean smtpStarttls = true;
	private boolean smtpEnableSsl = true;
	private boolean debug = false;
	
}
