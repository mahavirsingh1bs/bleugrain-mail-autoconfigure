package com.bleugrain.mail.autoconfigure;

import static java.lang.String.valueOf;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import com.carefarm.prakrati.security.Encryptor;

import lombok.extern.slf4j.Slf4j;

@Configuration
@ConditionalOnClass(JavaMailSender.class)
@ConditionalOnBean(Encryptor.class)
@EnableConfigurationProperties(MailProperties.class)
@Slf4j
public class BleugrainMailAutoConfiguration {
	
	@Autowired
	private MailProperties properties;
	
	@Autowired
	private Encryptor encryptor;
	
	@Bean
	@ConditionalOnMissingBean
	public JavaMailSender mailSender(Properties mailProperties) {
		log.info("defining mailSender bean");
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
		mailSender.setHost(encryptor.decrypt(properties.getHost()));
		mailSender.setPort(properties.getPort());
		mailSender.setUsername(encryptor.decrypt(properties.getUsername()));
		mailSender.setPassword(encryptor.decrypt(properties.getPassword()));
		mailSender.setJavaMailProperties(mailProperties);
		return mailSender;
	}
	
	@Bean
	@ConditionalOnMissingBean
	public Properties mailProperties() {
		log.info("defining mailProperties bean");
		Properties mailProperties = new Properties();
		mailProperties.setProperty("mail.transport.protocol", properties.getTransportProtocol());
		mailProperties.setProperty("mail.smtp.starttls.enable", valueOf(properties.isSmtpStarttls()));
		mailProperties.setProperty("mail.smtp.ssl.enable", valueOf(properties.isSmtpEnableSsl()));
		mailProperties.setProperty("mail.smtp.auth", valueOf(properties.isSmtpAuth()));
		mailProperties.setProperty("mail.debug", valueOf(properties.isDebug()));
		return mailProperties;
	}

	
}
